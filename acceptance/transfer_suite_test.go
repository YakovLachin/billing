package acceptance

import (
	"context"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/YakovLachin/billing/acceptance/helper"
	"gitlab.com/YakovLachin/billing/billing/account"
	billing_money "gitlab.com/YakovLachin/billing/billing/money"
	"gitlab.com/YakovLachin/billing/billing/server"
	"gitlab.com/YakovLachin/billing/billing/storage"
	"gitlab.com/YakovLachin/billing/billing/transaction"
	"google.golang.org/genproto/googleapis/type/money"
)

// Идендификатор транзакции для тестирования
var testTransferTx *transaction.Transaction
var testTransferSpenderAcc *account.Account
var testTransferRecipientAcc *account.Account

var _ = Describe("Transfer - Перевод средств.", func() {
	Context("Позитивный сценарий.", func() {
		It("Проверка баланса перед тестом", func() {
			acc, err := helper.GetTestAccWithBalance(&money.Money{Units: 200})
			Expect(err).Should(BeNil())
			testTransferSpenderAcc = acc
			acc2, err := helper.GetTestAccWithBalance(&money.Money{Units: 100})
			Expect(err).Should(BeNil())
			testTransferRecipientAcc = acc2
		})

		ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
		It("Должен принимать сообщение из входящей очереди и возвращать ответ в иcходящую очередь", func() {
			req := &server.AMQPTransactionRequest{
				RPCName: "transfer",
				Body: &transaction.Transaction{
					Amount: &money.Money{
						Units: 100,
					},
					SpenderAccount:   testTransferSpenderAcc,
					RecipientAccount: testTransferRecipientAcc,
				},
			}

			resp, err := helper.SendTXViaAMQP(ctx, req)
			Expect(err).Should(BeNil())
			Expect(resp.Body.GetID()).ShouldNot(BeEmpty())
			testTransferTx = &transaction.Transaction{
				ID: resp.Body.GetID(),
			}

		})

		It("Должна зафиксироваться транзакция на перевод средств с заданной суммой", func() {
			db, err := helper.GetDB()
			Expect(err).Should(BeNil())
			tx, err := storage.GetTransaction(db, &transaction.Transaction{ID: testTransferTx.GetID()})
			Expect(err).Should(BeNil())

			Expect(tx.GetAmount()).Should(Equal(&money.Money{Units: 100}))
			Expect(tx.GetStatus()).Should(Equal(transaction.TransactionStatusCommit))
			Expect(tx.GetType()).Should(Equal(transaction.TransactionTypeTransfer))
		})

		It("Баланс отправителя должен уменьшится на сумму перевода", func() {
			db, err := helper.GetDB()
			Expect(err).Should(BeNil())
			acc, err := storage.GetAccount(db, testTransferSpenderAcc)
			actual := billing_money.Diff(testTransferSpenderAcc.GetBalance(), &money.Money{Units: 100})
			expectedBalance := acc.GetBalance()
			Expect(expectedBalance).Should(Equal(actual))
		})

		It("Баланс получателя должен увеличится на сумму перевода", func() {
			db, err := helper.GetDB()
			Expect(err).Should(BeNil())
			acc, err := storage.GetAccount(db, testTransferRecipientAcc)
			actual := billing_money.Summ(testTransferRecipientAcc.GetBalance(), &money.Money{Units: 100})
			expectedBalance := acc.GetBalance()
			Expect(expectedBalance).Should(Equal(actual))
		})
	})
})
