package helper

import (
	"database/sql"
	"time"

	"gitlab.com/YakovLachin/billing/config"

	_ "github.com/go-sql-driver/mysql"
)

var DB *sql.DB

func GetDB() (*sql.DB, error) {
	if DB != nil {
		return DB, nil
	}

	mysqlDialURL, err := config.GetMYSQLDialURL()
	if err != nil {
		return nil, err
	}

	db, err := sql.Open("mysql", mysqlDialURL)
	if err != nil {
		return nil, err
	}

	for i := 10; i > 0; i-- {
		err = db.Ping()
		if err == nil {
			DB = db
			return DB, nil
		}
		time.Sleep(time.Second)
	}

	return nil, err
}
