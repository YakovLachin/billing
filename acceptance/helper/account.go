package helper

import (
	"fmt"

	"gitlab.com/YakovLachin/billing/billing/account"
	"gitlab.com/YakovLachin/billing/billing/storage"
	"gitlab.com/YakovLachin/billing/billing/storage/tests/helper"
	"gitlab.com/YakovLachin/billing/billing/transaction"
	"google.golang.org/genproto/googleapis/type/money"
)

func GetTestAccWithBalance(balance *money.Money) (*account.Account, error) {
	db, err := helper.GetDB()
	if err != nil {
		return nil, fmt.Errorf("fail to get database from helper: %s", err)
	}

	acc, err := storage.CreateAccount(db)
	if err != nil {
		return nil, fmt.Errorf("fail to get account from database: %s", err)
	}

	tx := &transaction.Transaction{
		Amount:           balance,
		Type:             transaction.TransactionTypeReciept,
		Status:           transaction.TransactionStatusCommit,
		RecipientAccount: acc,
	}

	_, err = storage.CreateTransaction(db, tx)
	if err != nil {
		return nil, err
	}

	acc.Balance = balance
	err = storage.UpdateAccount(db, acc)

	return acc, err
}
