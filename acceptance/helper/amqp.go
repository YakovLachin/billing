package helper

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/streadway/amqp"
	"gitlab.com/YakovLachin/billing/amqp/helper"
	"gitlab.com/YakovLachin/billing/billing/server"
)

func SendTXViaAMQP(ctx context.Context, req *server.AMQPTransactionRequest) (*server.AMQPTransactionResponse, error) {
	msg, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}

	amqpData, err := helper.AMQPForTest()
	if err != nil {
		return nil, err
	}
	defer func() {
		amqpData.Channel.Close()
	}()
	defer func() {
		amqpData.Connection.Close()
	}()

	err = amqpData.Channel.Publish(
		"",
		amqpData.In.Name,
		false,
		false,
		amqp.Publishing{
			Body: msg,
		},
	)
	if err != nil {
		return nil, err
	}

	msgs, err := amqpData.Channel.Consume(
		amqpData.Out.Name,
		"acceptance-test",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return nil, err
	}

	for {
		select {
		case delivery, ok := <-msgs:
			if !ok {
				continue
			}

			resp := &server.AMQPTransactionResponse{}
			err = json.Unmarshal(delivery.Body, resp)
			if err != nil {
				return nil, err
			}

			return resp, nil
		case <-ctx.Done():
			return nil, fmt.Errorf("context timeout is exceeded")
		}
	}
}

func SendAccViaAMQP(ctx context.Context, req *server.AMQPAccountRequest) (*server.AMQPAccountResponse, error) {
	msg, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}

	amqpData, err := helper.AMQPForTest()
	if err != nil {
		return nil, err
	}
	defer func() {
		amqpData.Channel.Close()
	}()
	defer func() {
		amqpData.Connection.Close()
	}()

	err = amqpData.Channel.Publish(
		"",
		amqpData.In.Name,
		false,
		false,
		amqp.Publishing{
			Body: msg,
		},
	)
	if err != nil {
		return nil, err
	}

	msgs, err := amqpData.Channel.Consume(
		amqpData.Out.Name,
		"acceptance-test",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return nil, err
	}

	for {
		select {
		case delivery, ok := <-msgs:
			if !ok {
				continue
			}

			resp := &server.AMQPAccountResponse{}
			err = json.Unmarshal(delivery.Body, resp)
			if err != nil {
				return nil, err
			}

			return resp, nil
		case <-ctx.Done():
			return nil, fmt.Errorf("context timeout is exceeded")
		}
	}
}
