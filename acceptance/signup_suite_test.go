package acceptance

import (
	"context"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/YakovLachin/billing/acceptance/helper"
	"gitlab.com/YakovLachin/billing/billing/account"
	"gitlab.com/YakovLachin/billing/billing/server"
	"gitlab.com/YakovLachin/billing/billing/storage"
)

var testAccForSignup *account.Account
var _ = Describe("Signup - Регистрация аккаунта.", func() {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	Context("", func() {
		It("Должен принимать сообщение из входящей очереди и возвращать ответ в иcходящую очередь", func() {
			req := &server.AMQPAccountRequest{
				RPCName: "signup",
			}

			resp, err := helper.SendAccViaAMQP(ctx, req)
			Expect(err).Should(BeNil())
			Expect(resp.Body.GetID()).ShouldNot(BeEmpty())
			testAccForSignup = &account.Account{
				ID: resp.Body.GetID(),
			}
		})

		It("Должен зарегистрироваться новый аккаунт", func() {
			db, err := helper.GetDB()
			Expect(err).Should(BeNil())
			acc, err := storage.GetAccount(db, &account.Account{ID: testAccForSignup.GetID()})
			Expect(err).Should(BeNil())
			Expect(acc.GetID().ToString()).ShouldNot(Equal(""))
			Expect(acc.GetID()).Should(Equal(testAccForSignup.GetID()))
		})
	})
})
