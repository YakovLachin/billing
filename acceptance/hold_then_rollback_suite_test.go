package acceptance

import (
	"context"
	"time"

	"gitlab.com/YakovLachin/billing/billing/account"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/YakovLachin/billing/acceptance/helper"
	billing_money "gitlab.com/YakovLachin/billing/billing/money"
	"gitlab.com/YakovLachin/billing/billing/server"
	"gitlab.com/YakovLachin/billing/billing/storage"
	"gitlab.com/YakovLachin/billing/billing/transaction"
	"google.golang.org/genproto/googleapis/type/money"
)

// Идендификатор транзакции для тестирования
var testHoldTx *transaction.Transaction
var testHoldSpenderAcc *account.Account
var testHoldRecipientAcc *account.Account

var _ = Describe("Transfer Hold then Rollback - Перевод c холдированием и последующей отменой.", func() {
	Context("Позитивный сценарий.", func() {
		It("Создаем тестовые аккаунты с балансом", func() {
			acc, err := helper.GetTestAccWithBalance(&money.Money{Units: 200})
			Expect(err).Should(BeNil())
			testHoldSpenderAcc = acc
			acc2, err := helper.GetTestAccWithBalance(&money.Money{Units: 100})
			Expect(err).Should(BeNil())
			testHoldRecipientAcc = acc2
		})

		ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
		It("Должен принимать сообщение из входящей очереди и возвращать ответ в иcходящую очередь", func() {
			req := &server.AMQPTransactionRequest{
				RPCName: "transfer_hold",
				Body: &transaction.Transaction{
					Amount: &money.Money{
						Units: 100,
					},
					SpenderAccount:   testHoldSpenderAcc,
					RecipientAccount: testHoldRecipientAcc,
				},
			}

			resp, err := helper.SendTXViaAMQP(ctx, req)
			Expect(err).Should(BeNil())
			Expect(resp.Body.GetID()).ShouldNot(BeEmpty())
			testHoldTx = &transaction.Transaction{
				ID: resp.Body.GetID(),
			}
		})

		It("Должна зафиксироваться транзакция на резевирование средств с заданной суммой", func() {
			db, err := helper.GetDB()
			Expect(err).Should(BeNil())
			tx, err := storage.GetTransaction(db, &transaction.Transaction{ID: testHoldTx.GetID()})
			Expect(err).Should(BeNil())

			Expect(tx.GetAmount()).Should(Equal(&money.Money{Units: 100}))
			Expect(tx.GetStatus()).Should(Equal(transaction.TransactionStatusCommit))
			Expect(tx.GetType()).Should(Equal(transaction.TransactionTypeTransferHold))
		})

		It("Баланс отправителя должен уменьшится на сумму зарезервированных средств", func() {
			db, err := helper.GetDB()
			Expect(err).Should(BeNil())
			acc, err := storage.GetAccount(db, testHoldSpenderAcc)
			actual := billing_money.Diff(testHoldSpenderAcc.GetBalance(), &money.Money{Units: 100})
			expectedBalance := acc.GetBalance()
			Expect(expectedBalance).Should(Equal(actual))
		})

		It("Баланс получателя должен не изменится", func() {
			db, err := helper.GetDB()
			Expect(err).Should(BeNil())
			acc, err := storage.GetAccount(db, testHoldRecipientAcc)
			actual := testHoldRecipientAcc.GetBalance()
			expectedBalance := acc.GetBalance()
			Expect(expectedBalance).Should(Equal(actual))
		})

		It("Должен принимать сообщение из входящей очереди и возвращать ответ в иcходящую очередь", func() {
			req := &server.AMQPTransactionRequest{
				RPCName: "transfer_rollback",
				Body: &transaction.Transaction{
					ID: testHoldTx.GetID(),
					Amount: &money.Money{
						Units: 100,
					},
					SpenderAccount:   testHoldSpenderAcc,
					RecipientAccount: testHoldRecipientAcc,
				},
			}

			resp, err := helper.SendTXViaAMQP(ctx, req)
			Expect(err).Should(BeNil())
			Expect(resp.Body.GetID()).Should(Equal(testHoldTx.GetID()))
		})

		It("Транзакция на резевирование средств должна изменить статус на отменено - rollback", func() {
			db, err := helper.GetDB()
			Expect(err).Should(BeNil())
			tx, err := storage.GetTransaction(db, &transaction.Transaction{ID: testHoldTx.GetID()})
			Expect(err).Should(BeNil())

			Expect(tx.GetAmount()).Should(Equal(&money.Money{Units: 100}))
			Expect(tx.GetStatus()).Should(Equal(transaction.TransactionStatusRollback))
			Expect(tx.GetType()).Should(Equal(transaction.TransactionTypeTransferHold))
		})

		It("Баланс отправителя должен обратно увеличиться на сумму зарезервированных средств", func() {
			db, err := helper.GetDB()
			Expect(err).Should(BeNil())
			acc, err := storage.GetAccount(db, testHoldSpenderAcc)
			actual := testHoldSpenderAcc.GetBalance()
			expectedBalance := acc.GetBalance()
			Expect(expectedBalance).Should(Equal(actual))
		})

		It("Баланс получателя должен не изменится", func() {
			db, err := helper.GetDB()
			Expect(err).Should(BeNil())
			acc, err := storage.GetAccount(db, testHoldRecipientAcc)
			actual := testHoldRecipientAcc.GetBalance()
			expectedBalance := acc.GetBalance()
			Expect(expectedBalance).Should(Equal(actual))
		})
	})
})
