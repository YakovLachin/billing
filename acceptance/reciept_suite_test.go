package acceptance

import (
	"context"
	"testing"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/YakovLachin/billing/acceptance/helper"
	"gitlab.com/YakovLachin/billing/billing/account"
	billing_money "gitlab.com/YakovLachin/billing/billing/money"
	"gitlab.com/YakovLachin/billing/billing/server"
	"gitlab.com/YakovLachin/billing/billing/storage"
	"gitlab.com/YakovLachin/billing/billing/transaction"

	"google.golang.org/genproto/googleapis/type/money"
)

func TestSuite(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Функциональность сервиса")
}

// Идендификатор транзакции для тестирования
var testRecieptTx *transaction.Transaction
var testRecieptAcc *account.Account

var _ = Describe("Reciept - Зачисление средств.", func() {
	Context("Позитивный сценарий.", func() {
		It("Проверка баланса перед тестом", func() {
			acc, err := helper.GetTestAccWithBalance(&money.Money{Units: 100})
			Expect(err).Should(BeNil())
			testRecieptAcc = acc
		})

		ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
		It("Должен принимать сообщение из входящей очереди и возвращать ответ в иcходящую очередь", func() {
			req := &server.AMQPTransactionRequest{
				RPCName: "reciept",
				Body: &transaction.Transaction{
					Amount: &money.Money{
						Units: 100,
					},
					RecipientAccount: testRecieptAcc,
				},
			}

			resp, err := helper.SendTXViaAMQP(ctx, req)
			Expect(err).Should(BeNil())
			Expect(resp.Body.GetID()).ShouldNot(BeEmpty())
			testRecieptTx = &transaction.Transaction{
				ID: resp.Body.GetID(),
			}
		})

		It("Должна зафиксироваться транзакция на пополнение баланса с заданной суммой пополнения", func() {
			db, err := helper.GetDB()
			Expect(err).Should(BeNil())
			tx, err := storage.GetTransaction(db, &transaction.Transaction{ID: testRecieptTx.GetID()})
			Expect(err).Should(BeNil())

			Expect(tx.GetAmount()).Should(Equal(&money.Money{Units: 100}))
			Expect(tx.GetStatus()).Should(Equal(transaction.TransactionStatusCommit))
			Expect(tx.GetType()).Should(Equal(transaction.TransactionTypeReciept))
		})

		It("При зачислении баланс пользователя должен увеличится на сумму пополнения", func() {
			db, err := helper.GetDB()
			Expect(err).Should(BeNil())
			acc, err := storage.GetAccount(db, testRecieptAcc)
			actual := billing_money.Summ(testRecieptAcc.GetBalance(), &money.Money{Units: 100})
			expectedBalance := acc.GetBalance()
			Expect(expectedBalance).Should(Equal(actual))
		})
	})
})
