package acceptance

import (
	"context"
	"time"

	"gitlab.com/YakovLachin/billing/billing/account"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/YakovLachin/billing/acceptance/helper"
	billing_money "gitlab.com/YakovLachin/billing/billing/money"
	"gitlab.com/YakovLachin/billing/billing/server"
	"gitlab.com/YakovLachin/billing/billing/storage"
	"gitlab.com/YakovLachin/billing/billing/transaction"
	"google.golang.org/genproto/googleapis/type/money"
)

// Идендификатор транзакции для тестирования
var testSpendTx *transaction.Transaction
var testSpendAcc *account.Account

var _ = Describe("Spend - Списание средств.", func() {
	Context("Позитивный сценарий.", func() {
		It("Проверка баланса перед тестом", func() {
			acc2, err := helper.GetTestAccWithBalance(&money.Money{Units: 200})
			Expect(err).Should(BeNil())
			testSpendAcc = acc2
		})

		ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

		It("Должен принимать сообщение из входящей очереди и возвращать ответ в иcходящую очередь", func() {
			req := &server.AMQPTransactionRequest{
				RPCName: "spend",
				Body: &transaction.Transaction{
					Amount: &money.Money{
						Units: 100,
					},
					SpenderAccount: testSpendAcc,
				},
			}

			resp, err := helper.SendTXViaAMQP(ctx, req)
			Expect(err).Should(BeNil())
			Expect(resp.Body.GetID()).ShouldNot(BeEmpty())
			testSpendTx = &transaction.Transaction{
				ID: resp.Body.GetID(),
			}
		})

		It("Должна зафиксироваться транзакция на списание баланса с заданной суммой пополнения", func() {
			db, err := helper.GetDB()
			Expect(err).Should(BeNil())
			tx, err := storage.GetTransaction(db, &transaction.Transaction{ID: testSpendTx.GetID()})
			Expect(err).Should(BeNil())

			Expect(tx.GetAmount()).Should(Equal(&money.Money{Units: 100}))
			Expect(tx.GetStatus()).Should(Equal(transaction.TransactionStatusCommit))
			Expect(tx.GetType()).Should(Equal(transaction.TransactionTypeSpend))
		})

		It("При зачислении баланс пользователя должен уменьшится на сумму пополнения", func() {
			db, err := helper.GetDB()
			Expect(err).Should(BeNil())
			acc, err := storage.GetAccount(db, testSpendAcc)
			actual := billing_money.Diff(testSpendAcc.GetBalance(), &money.Money{Units: 100})
			expectedBalance := acc.GetBalance()
			Expect(expectedBalance).Should(Equal(actual))
		})
	})
})
