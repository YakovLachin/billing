package acceptance

import (
	"context"
	"time"

	"gitlab.com/YakovLachin/billing/billing/account"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/YakovLachin/billing/acceptance/helper"
	billing_money "gitlab.com/YakovLachin/billing/billing/money"
	"gitlab.com/YakovLachin/billing/billing/server"
	"gitlab.com/YakovLachin/billing/billing/storage"
	"gitlab.com/YakovLachin/billing/billing/transaction"
	"google.golang.org/genproto/googleapis/type/money"
)

// Идендификатор транзакции для тестирования
var testTransferHoldTx *transaction.Transaction
var testTransferHoldSpenderAcc *account.Account
var testTransferHoldRecipientAcc *account.Account

var _ = Describe("Transfer Hold then Confirm - Перевод c холдированием и последующим подтверждением.", func() {
	Context("Позитивный сценарий.", func() {
		It("Создаем тестовые аккаунты с балансом", func() {
			acc, err := helper.GetTestAccWithBalance(&money.Money{Units: 200})
			Expect(err).Should(BeNil())
			testTransferHoldSpenderAcc = acc
			acc2, err := helper.GetTestAccWithBalance(&money.Money{Units: 100})
			Expect(err).Should(BeNil())
			testTransferHoldRecipientAcc = acc2
		})

		ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
		It("Должен принимать сообщение из входящей очереди и возвращать ответ в иcходящую очередь", func() {
			req := &server.AMQPTransactionRequest{
				RPCName: "transfer_hold",
				Body: &transaction.Transaction{
					Amount: &money.Money{
						Units: 100,
					},
					SpenderAccount:   testTransferHoldSpenderAcc,
					RecipientAccount: testTransferHoldRecipientAcc,
				},
			}

			resp, err := helper.SendTXViaAMQP(ctx, req)
			Expect(err).Should(BeNil())
			Expect(resp.Body.GetID()).ShouldNot(BeEmpty())
			testTransferHoldTx = &transaction.Transaction{
				ID: resp.Body.GetID(),
			}
		})

		It("Должна зафиксироваться транзакция на резевирование средств с заданной суммой", func() {
			db, err := helper.GetDB()
			Expect(err).Should(BeNil())
			tx, err := storage.GetTransaction(db, &transaction.Transaction{ID: testTransferHoldTx.GetID()})
			Expect(err).Should(BeNil())

			Expect(tx.GetAmount()).Should(Equal(&money.Money{Units: 100}))
			Expect(tx.GetStatus()).Should(Equal(transaction.TransactionStatusCommit))
			Expect(tx.GetType()).Should(Equal(transaction.TransactionTypeTransferHold))
		})

		It("Баланс отправителя должен уменьшится на сумму зарезервированных средств", func() {
			db, err := helper.GetDB()
			Expect(err).Should(BeNil())
			acc, err := storage.GetAccount(db, testTransferHoldSpenderAcc)
			actual := billing_money.Diff(testTransferHoldSpenderAcc.GetBalance(), &money.Money{Units: 100})
			expectedBalance := acc.GetBalance()
			Expect(expectedBalance).Should(Equal(actual))
		})

		It("Баланс получателя должен не изменится", func() {
			db, err := helper.GetDB()
			Expect(err).Should(BeNil())
			acc, err := storage.GetAccount(db, testTransferHoldRecipientAcc)
			actual := testTransferHoldRecipientAcc.GetBalance()
			expectedBalance := acc.GetBalance()
			Expect(expectedBalance).Should(Equal(actual))
		})

		It("Должен принимать сообщение из входящей очереди и возвращать ответ в иcходящую очередь", func() {
			req := &server.AMQPTransactionRequest{
				RPCName: "transfer_confirm",
				Body: &transaction.Transaction{
					ID: testTransferHoldTx.GetID(),
					Amount: &money.Money{
						Units: 100,
					},
					SpenderAccount:   testTransferHoldSpenderAcc,
					RecipientAccount: testTransferHoldRecipientAcc,
				},
			}

			resp, err := helper.SendTXViaAMQP(ctx, req)
			Expect(err).Should(BeNil())
			Expect(resp.Body.GetID()).Should(Equal(testTransferHoldTx.GetID()))
		})

		It("Транзакция на резевирование средств должна перевестись в тип перевод", func() {
			db, err := helper.GetDB()
			Expect(err).Should(BeNil())
			tx, err := storage.GetTransaction(db, &transaction.Transaction{ID: testTransferHoldTx.GetID()})
			Expect(err).Should(BeNil())

			Expect(tx.GetAmount()).Should(Equal(&money.Money{Units: 100}))
			Expect(tx.GetStatus()).Should(Equal(transaction.TransactionStatusCommit))
			Expect(tx.GetType()).Should(Equal(transaction.TransactionTypeTransfer))
		})

		It("Баланс отправителя оставаться преждней уменьшенной суммой", func() {
			db, err := helper.GetDB()
			Expect(err).Should(BeNil())
			acc, err := storage.GetAccount(db, testTransferHoldSpenderAcc)
			actual := billing_money.Diff(testTransferHoldSpenderAcc.GetBalance(), &money.Money{Units: 100})
			expectedBalance := acc.GetBalance()
			Expect(expectedBalance).Should(Equal(actual))
		})

		It("Баланс получателя должен увеличится на сумму перевода", func() {
			db, err := helper.GetDB()
			Expect(err).Should(BeNil())
			acc, err := storage.GetAccount(db, testTransferHoldRecipientAcc)
			actual := billing_money.Summ(testTransferHoldRecipientAcc.GetBalance(), &money.Money{Units: 100})
			expectedBalance := acc.GetBalance()
			Expect(expectedBalance).Should(Equal(actual))
		})
	})
})
