package main

import (
	"log"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/YakovLachin/billing/amqp"
	"gitlab.com/YakovLachin/billing/billing"
	"gitlab.com/YakovLachin/billing/billing/server"
	"gitlab.com/YakovLachin/billing/config"
)

func main() {
	dialURL, err := config.GetRabbitMQDialURL()
	log.Print(dialURL)
	if err != nil {
		log.Fatalf("%s: %s", "fail to get dial URL for rabbitmq", err)
	}

	mysqlDialURL, err := config.GetMYSQLDialURL()
	if err != nil {
		log.Fatalf("%s: %s", "fail to get dial URL for mysql", err)
	}

	db, err := TryOpenSQL(mysqlDialURL)
	if err != nil {
		log.Fatalf("Fail connect to myslq %s", err)
	}
	done := make(chan struct{})
	inputQueue := config.GetInputQueueName()
	outputQueue := config.GetOutputQueueName()
	errOutputQueue := config.GetErrorOutputQueueName()
	forever := make(chan bool)
	go SqlHealthCheck(db, mysqlDialURL, done)
	m := amqp.NewServerMux()
	m.HandleFunc("spend", server.AMQPHandleTxFunc(db, billing.Spend))
	m.HandleFunc("reciept", server.AMQPHandleTxFunc(db, billing.Reciept))
	m.HandleFunc("transfer", server.AMQPHandleTxFunc(db, billing.Transfer))
	m.HandleFunc("transfer_hold", server.AMQPHandleTxFunc(db, billing.TransferHold))
	m.HandleFunc("transfer_confirm", server.AMQPHandleTxFunc(db, billing.TransferConfirm))
	m.HandleFunc("transfer_rollback", server.AMQPHandleTxFunc(db, billing.TransferRollback))
	m.HandleFunc("signup", server.AMQPHandleAccountFunc(db, billing.Signup))

	err = amqp.ListenAndServe(dialURL, inputQueue, outputQueue, m, amqp.WithErrorQueue(errOutputQueue))
	if err != nil {
		close(forever)
		log.Fatalf("fail to listen amqp connections: %s", err)
	}

	log.Print("start listening")
	<-forever
}
