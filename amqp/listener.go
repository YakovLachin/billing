package amqp

import "github.com/streadway/amqp"

type readWriter struct {
	connection  *amqp.Connection
	channel     *amqp.Channel
	inputQueue  amqp.Queue
	outputQueue amqp.Queue
	errorQueue  amqp.Queue
}

func (r *readWriter) Read() (<-chan amqp.Delivery, error) {
	return r.channel.Consume(
		r.inputQueue.Name,
		"",
		false,
		false,
		false,
		false,
		nil,
	)
}

func (r *readWriter) Write(data []byte) error {
	return r.channel.Publish(
		"",
		r.outputQueue.Name,
		false,
		false,
		amqp.Publishing{
			Body: data,
		},
	)
}

func (r *readWriter) ErrorWrite(data []byte) error {
	if r.errorQueue.Name == "" {
		return nil
	}

	return r.channel.Publish(
		"",
		r.errorQueue.Name,
		false,
		false,
		amqp.Publishing{
			Body: data,
		},
	)
}

func (r *readWriter) Close() {
	r.channel.Close()
	r.connection.Close()
}

func (r *readWriter) ConnectionNotifyClose() chan *amqp.Error {
	return r.connection.NotifyClose(make(chan *amqp.Error))
}

func (s *Srv) Listen() (*readWriter, error) {
	conn, err := amqp.Dial(s.url)
	if err != nil {
		return nil, err
	}

	channel, err := conn.Channel()
	if err != nil {
		return nil, err
	}

	defer func() {
		if err != nil {
			channel.Close()
			conn.Close()
		}
	}()

	l := &readWriter{
		connection: conn,
		channel:    channel,
	}

	requestQueue, err := channel.QueueDeclare(
		s.requestQueueName,
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return nil, err
	}

	l.inputQueue = requestQueue

	responseQueue, err := channel.QueueDeclare(
		s.responseQueueName,
		false,
		false,
		false,
		false,
		nil,
	)

	l.outputQueue = responseQueue
	if s.errorQueueName != "" {
		errorResponseQueue, err := channel.QueueDeclare(
			s.errorQueueName,
			false,
			false,
			false,
			false,
			nil,
		)
		if err != nil {
			return nil, err
		}

		l.errorQueue = errorResponseQueue
	}

	return l, nil
}
