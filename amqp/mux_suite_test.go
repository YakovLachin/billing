package amqp

import (
	"encoding/json"
	"fmt"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestSime(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Работа с amqp mux")
}

var _ = Describe("Критерии работоспособности сервера", func() {
	Context("Критерии работспособности mux", func() {
		It("mux должен подключать обработчики по нужному имени метода", func() {
			m := NewServerMux()
			m.HandleFunc("methodName", func(bytes []byte) ([]byte, error) {
				return []byte{}, nil
			})

			Expect(m.methods).Should(HaveKey("methodName"))
		})

		It("На вызов по имени метода должен вызывать соответсвующий обработчик", func() {
			m := NewServerMux()
			var handlerIsInvoked bool
			m.HandleFunc("methodName", func(bytes []byte) ([]byte, error) {
				handlerIsInvoked = true
				return []byte{}, nil
			})

			r := Request{
				RPCName: "methodName",
			}
			rawData, err := json.Marshal(&r)
			Expect(err).Should(BeNil())
			m.Handle(rawData)

			Expect(handlerIsInvoked).Should(BeTrue())
		})

		It("Должен возвращать выброшеные обработчиком ошибки", func() {
			m := NewServerMux()
			expectedErr := fmt.Errorf("Эта ошибка должна вернуться от отобработчика")
			m.HandleFunc("methodName", func(bytes []byte) ([]byte, error) {

				return []byte{}, expectedErr
			})

			r := Request{
				RPCName: "methodName",
			}
			rawData, _ := json.Marshal(&r)
			_, err := m.Handle(rawData)

			Expect(err).Should(Equal(expectedErr))
		})

		It("Должен возвращать ошибку, при вызове метода, который не обрабатывается", func() {
			m := NewServerMux()
			r := Request{
				RPCName: "methodName",
			}
			rawData, _ := json.Marshal(&r)
			_, err := m.Handle(rawData)
			expectedErr := fmt.Errorf(`fail to handle: methodName, method: methodName not found`)
			Expect(err).Should(Equal(expectedErr))
		})

		It("Должен вернуть ответ от обработчика", func() {
			m := NewServerMux()
			m.HandleFunc("methodName", func(bytes []byte) ([]byte, error) {
				return []byte("methodName response"), nil
			})

			r := Request{
				RPCName: "methodName",
			}
			rawData, err := json.Marshal(&r)
			Expect(err).Should(BeNil())
			response, _ := m.Handle(rawData)

			Expect(response).Should(Equal([]byte(`methodName response`)))
		})
	})
})
