package amqp

import (
	"log"
	"time"
)

type HandlerInterface interface {
	Handle([]byte) ([]byte, error)
}

type Srv struct {
	url               string
	requestQueueName  string
	responseQueueName string
	errorQueueName    string
	handler           HandlerInterface
	doneChannel       <-chan struct{}
}

type Option func(s *Srv)

// Опция добавляет очередь для ошибок
func WithErrorQueue(queue string) Option {
	return func(s *Srv) {
		s.errorQueueName = queue
	}
}

func ListenAndServe(dialURL, requestQueueName, responseQueueName string, mux HandlerInterface, options ...Option) error {
	s := &Srv{
		url:               dialURL,
		responseQueueName: responseQueueName,
		requestQueueName:  requestQueueName,
		handler:           mux,
	}

	for _, option := range options {
		option(s)
	}

	return s.ListenAndServe()
}

func (s *Srv) ListenAndServe() error {
	l, err := s.Listen()
	if err != nil {
		return err
	}

	go s.Serve(l)
	return nil
}

func (s *Srv) Serve(l *readWriter) {
	msgs, err := l.Read()
	if err != nil {
		return
	}

	amqpConnectionIsClosed := l.ConnectionNotifyClose()
	for {
		select {
		case <-s.doneChannel:
			l.Close()
			return
		case <-amqpConnectionIsClosed:
			log.Printf("Connection is closed")
			s.RetryListenAndServe()
			return
		case d, ok := <-msgs:
			if !ok {
				continue
			}

			responseBody, err := s.handler.Handle(d.Body)
			if err != nil {
				log.Printf("server handle err: %s", err)
				errWrite := l.ErrorWrite([]byte(err.Error()))
				if errWrite != nil {
					log.Printf("failt to error report: %s", errWrite)
				}
				err = d.Ack(false)
				continue
			}

			err = l.Write(responseBody)
			if err != nil {
				log.Printf("publish message  err: %s", err)
				errWrite := l.ErrorWrite([]byte(err.Error()))
				if errWrite != nil {
					log.Printf("failt to error report: %s", errWrite)
				}
			}

			err = d.Ack(false)
			if err != nil {
				log.Printf("message ack err: %s", err)
				errWrite := l.ErrorWrite([]byte(err.Error()))
				if errWrite != nil {
					log.Printf("failt to error report: %s", errWrite)
				}
			}
		}
	}
}

func (s *Srv) RetryListenAndServe() {
	for {
		time.Sleep(2 * time.Second)
		l, err := s.Listen()
		if err != nil {
			log.Printf("fail to retrying listen amqp : %s", err)
			continue
		}
		go s.Serve(l)
		log.Printf("retry connect listean and serve amqp is success")
		return
	}
}
