package amqp

import (
	"encoding/json"
	"fmt"
)

type HandleFunc func([]byte) ([]byte, error)

type Handler struct {
	methods map[string]HandleFunc
}

type Request struct {
	RPCName string      `json:"rpc_name"`
	Body    interface{} `json:"body"`
}

func (h *Handler) HandleFunc(RPCName string, f HandleFunc) {
	if RPCName == "" {
		panic("amqp: rpc pattern")
	}
	if f == nil {
		panic("amqp: nil handler")
	}
	if _, exist := h.methods[RPCName]; exist {
		panic("amqp: multiple registrations for " + RPCName)
	}

	h.methods[RPCName] = f
}

func (h *Handler) Handle(rawRequest []byte) ([]byte, error) {
	r := &Request{}
	err := json.Unmarshal(rawRequest, r)
	if err != nil {
		return []byte{}, fmt.Errorf("fail to unmarshal to Request: %s", err)
	}
	handleFunc, ok := h.methods[r.RPCName]
	if !ok {
		return []byte{}, fmt.Errorf("fail to handle: %s, method: %s not found", r.RPCName, r.RPCName)
	}

	return handleFunc(rawRequest)
}

func NewServerMux() *Handler {
	h := &Handler{
		methods: make(map[string]HandleFunc),
	}

	return h
}
