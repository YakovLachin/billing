package helper

import (
	"fmt"

	"github.com/streadway/amqp"
	"gitlab.com/YakovLachin/billing/config"
)

type AMQP struct {
	In         amqp.Queue
	Out        amqp.Queue
	Channel    *amqp.Channel
	DialURL    string
	Connection *amqp.Connection
}

func AMQPForTest() (*AMQP, error) {
	inputQueue := config.GetInputQueueName()
	outputQueue := config.GetOutputQueueName()
	dialURL, err := config.GetRabbitMQDialURL()
	if err != nil {
		return nil, fmt.Errorf("bad Config: %s", err)
	}
	conn, err := amqp.Dial(dialURL)
	if err != nil {
		return nil, fmt.Errorf("err to create connection: %s", err)
	}
	ch, err := conn.Channel()
	if err != nil {
		return nil, fmt.Errorf("err to create channel: %s", err)
	}
	in, err := ch.QueueDeclare(
		inputQueue,
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return nil, err
	}
	out, err := ch.QueueDeclare(
		outputQueue,
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return nil, err
	}
	return &AMQP{
		In:         in,
		Out:        out,
		Channel:    ch,
		DialURL:    dialURL,
		Connection: conn,
	}, nil
}
