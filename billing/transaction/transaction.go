package transaction

import (
	"gitlab.com/YakovLachin/billing/billing/account"
	"gitlab.com/YakovLachin/billing/billing/uuid"
	"google.golang.org/genproto/googleapis/type/money"
)

const (
	TransactionTypeSpend         = "spend"
	TransactionTypeReciept       = "reciept"
	TransactionTypeTransfer      = "transfer"
	TransactionTypeTransferHold  = "transfer_hold"
	TransactionStatusProcess     = "process"
	TransactionStatusCommit      = "commit"
	TransactionStatusRollback    = "rollback"
	TransactionStatusErrorCancel = "error_cancel"
)

// Структура транзакции
type Transaction struct {
	// Уникальный идендификатор
	ID uuid.UID `json:"id"`
	// Сумма средств
	Amount *money.Money `json:"amount"`
	// Тип транзакции Зачисление, Списание, Перевод, Перевод с холдированием
	Type string `json:"type"`
	// Статус
	Status string `json:"status"`
	// Аккаунт Отправителя
	SpenderAccount *account.Account `json:"spender_account"`
	// Аккаунт принимающего
	RecipientAccount *account.Account `json:"recipient_account"`
	// Описание
	Description string `json:"description"`
}

func (t *Transaction) GetID() uuid.UID {
	if t == nil {
		return uuid.UID("")
	}

	return t.ID
}

func (t *Transaction) GetAmount() *money.Money {
	if t == nil {
		return nil
	}

	return t.Amount
}

func (t *Transaction) GetSpenderAccount() *account.Account {
	if t == nil {
		return nil
	}

	return t.SpenderAccount
}

func (t *Transaction) GetRecipientAccount() *account.Account {
	if t == nil {
		return nil
	}

	return t.RecipientAccount
}

func (t *Transaction) GetStatus() string {
	if t == nil {
		return ""
	}

	return t.Status
}

func (t *Transaction) GetDescription() string {
	if t == nil {
		return ""
	}

	return t.Description
}

func (t *Transaction) GetType() string {
	if t == nil {
		return ""
	}

	return t.Type
}
