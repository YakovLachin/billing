package money

import (
	gmoney "google.golang.org/genproto/googleapis/type/money"
)

const Nano = 1
const NanosInUnit = 1000000000 * Nano

// Возвращает сумму первого и второго числа
func Summ(m1 *gmoney.Money, m2 *gmoney.Money) *gmoney.Money {
	gmRes := &gmoney.Money{
		Units: m1.Units + m2.Units,
		Nanos: m1.Nanos + m2.Nanos,
	}

	Normalize(gmRes)
	return gmRes
}


// Возвращает разницу между первым и вторым числом
func Diff(m1 *gmoney.Money, m2 *gmoney.Money) *gmoney.Money {
	gmRes := &gmoney.Money{
		Units: m1.Units - m2.Units,
		Nanos: m1.Nanos - m2.Nanos,
	}

	Normalize(gmRes)

	return gmRes
}

// Приводит в нормальную тип gmMoney после суммирования или разницы
func Normalize(m *gmoney.Money) {
	if m.Nanos >= NanosInUnit {
		m.Units++
		m.Nanos = m.Nanos - NanosInUnit
	}

	if m.Nanos < 0 && m.Units > 0 {
		m.Units--
		m.Nanos = NanosInUnit + m.Nanos
	}

	if m.Nanos <= -NanosInUnit {
		m.Units--
		m.Nanos = m.Nanos + NanosInUnit
	}

	if m.Nanos > 0 && m.Units < 0 {
		m.Units++
		m.Nanos = m.Nanos - NanosInUnit
	}
}

// true Если первый аргумент больше второго
func GreaterThan(m1 *gmoney.Money, m2 *gmoney.Money) bool {
	if m1.Units > m2.Units {
		return true
	}

	if m1.Units < m2.Units {
		return false
	}

	if m1.Nanos > m2.Nanos {
		return true
	}

	return false
}
