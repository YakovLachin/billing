package money

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"google.golang.org/genproto/googleapis/type/money"
)

func TestDiff_FirstMoreThanSecond_ReturnPositive(t *testing.T) {
	first := &money.Money{
		Units: 200,
		Nanos: 100000000,
	}
	second := &money.Money{
		Units: 100,
		Nanos: 100000000,
	}

	actual := &money.Money{
		Units: 100,
	}

	expected := Diff(first, second)
	assert.Equal(t, expected, actual)
}

func TestDiff_FirstLessThanSecond_ReturnNegative(t *testing.T) {
	first := &money.Money{
		Units: 100,
		Nanos: 100000000,
	}
	second := &money.Money{
		Units: 200,
		Nanos: 100000000,
	}

	actual := &money.Money{
		Units: -100,
	}

	expected := Diff(first, second)
	assert.Equal(t, expected, actual)
}

func TestDiff_FirstIsIntSecondIsFloat_ReturnFloat(t *testing.T) {
	first := &money.Money{
		Units: 1,
	}
	second := &money.Money{
		Units: 0,
		Nanos: 100000000,
	}

	actual := &money.Money{
		Units: 0,
		Nanos: 900000000,
	}

	expected := Diff(first, second)
	assert.Equal(t, expected, actual)
}

func TestDiff_FirstLessThanSecond2_ReturnsNegative(t *testing.T) {
	first := &money.Money{
		Units: 1,
		Nanos: 100000000,
	}
	second := &money.Money{
		Units: 1,
		Nanos: 200000000,
	}

	actual := &money.Money{
		Units: 0,
		Nanos: -100000000,
	}

	expected := Diff(first, second)
	assert.Equal(t, expected, actual)
}

func TestDiff_FirstLessThanSecond_ReturnsNegative(t *testing.T) {
	first := &money.Money{
		Units: 1,
		Nanos: 200000000,
	}
	second := &money.Money{
		Units: 2,
		Nanos: 100000000,
	}

	actual := &money.Money{
		Units: 0,
		Nanos: -900000000,
	}

	expected := Diff(first, second)
	assert.Equal(t, expected, actual)
}

func TestSumm(t *testing.T) {
	first := &money.Money{
		Units: 1,
		Nanos: 900000000,
	}
	second := &money.Money{
		Units: 1,
		Nanos: 200000000,
	}

	actualSumm := &money.Money{
		Units: 3,
		Nanos: 100000000,
	}

	expected := Summ(first, second)
	assert.Equal(t, expected, actualSumm)
}

func TestGreaterThan_FirstUnitsGreaterThanSecond_ReturnsTrue(t *testing.T) {
	m1 := &money.Money{
		Units: 2,
	}
	m2 := &money.Money{
		Units: 1,
	}
	assert.True(t, GreaterThan(m1, m2))
}

func TestGreaterThan_FirstUnitsLessThanSecond_ReturnsFalse(t *testing.T) {
	m1 := &money.Money{
		Units: 1,
		Nanos: 2,
	}
	m2 := &money.Money{
		Units: 2,
		Nanos: 1,
	}
	assert.False(t, GreaterThan(m1, m2))
}

func TestGreaterThan_FirstUnitsEqualsToSecondUnits_ReturnsFalse(t *testing.T) {
	m1 := &money.Money{
		Units: 1,
	}
	m2 := &money.Money{
		Units: 1,
	}
	assert.False(t, GreaterThan(m1, m2))
}

func TestGreaterThan_FirstNanosGreaterThanSecond_ReturnsTrue(t *testing.T) {
	m1 := &money.Money{
		Units: 1,
		Nanos: 2,
	}
	m2 := &money.Money{
		Units: 1,
		Nanos: 1,
	}
	assert.True(t, GreaterThan(m1, m2))
}

func TestGreaterThan_ZeroAndZero_ReturnsFalse(t *testing.T) {
	assert.False(t, GreaterThan(&money.Money{}, &money.Money{}))
}
