package billing

import (
	"database/sql"
	"encoding/json"
	"fmt"

	"gitlab.com/YakovLachin/billing/billing/account"
	billing_money "gitlab.com/YakovLachin/billing/billing/money"
	"gitlab.com/YakovLachin/billing/billing/storage"
	"gitlab.com/YakovLachin/billing/billing/transaction"
	"google.golang.org/genproto/googleapis/type/money"
)

//
func commitTransaction(db *sql.DB, prepared *transaction.Transaction) (err error) {
	if prepared.GetSpenderAccount() == nil && prepared.GetRecipientAccount() == nil {
		return fmt.Errorf(
			"transaction with id(%s): sender and recipient is empty",
			prepared.ID.ToString(),
		)
	}

	tx, txErr := db.Begin()
	if txErr != nil {
		err = fmt.Errorf(
			"transaction with id(%s): fail to begin transaction: %s",
			prepared.ID.ToString(),
			txErr,
		)
	}

	defer func() {
		if err != nil {
			rollBackErr := tx.Rollback()
			if rollBackErr != nil {
				err = fmt.Errorf("fail rollback transaction %s, after error (%s)", rollBackErr, err)
			}
		}
	}()

	if prepared.GetSpenderAccount() != nil {
		err := changeAccountBalance(tx, prepared.GetSpenderAccount(), prepared)
		if err != nil {
			return fmt.Errorf(
				"fail to commit transaction by id(%s): fail to recalculate spender balanse: %s",
				prepared.ID.ToString(),
				err,
			)
		}
	}

	if prepared.GetRecipientAccount() != nil {
		err := changeAccountBalance(tx, prepared.GetRecipientAccount(), prepared)
		if err != nil {
			return fmt.Errorf(
				"fail to commit transaction by id(%s): fail to recalculate recipient balanse: %s",
				prepared.ID.ToString(),
				err,
			)
		}
	}

	prepared.Status = transaction.TransactionStatusCommit
	err = storage.UpdateTransaction(tx, prepared)
	if err != nil {
		return fmt.Errorf("fail to update transaction with id(%s) to status commit: %s", prepared.ID.ToString(), err)
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("fial to commit transaction with id(%s): %s", prepared.ID.ToString(), err)
	}

	return nil
}

// Записывает новое состояние баланся с учетом всех зафиксированных транзакций и дополнительной
// тразакции. Дополнительная транзакция должа быть зафиксирована после изменения состояния.
func changeAccountBalance(db *sql.Tx, acc *account.Account, tx *transaction.Transaction) error {
	txs, err := storage.ListTransactions(db, acc.GetID())
	if err != nil {
		return fmt.Errorf(
			"fail to list transaction by account id (%s): %s",
			acc.ID.ToString(),
			err,
		)
	}

	txs = append(txs, tx)
	newUserBalance := calculate(acc, txs)
	if billing_money.GreaterThan(&money.Money{}, newUserBalance) {
		m, err := json.Marshal(&txs)
		if err != nil {
			m = []byte("error string")
		}

		return fmt.Errorf("low Balance. New Balance is: %v, %s", *newUserBalance, string(m))
	}

	acc.Balance = newUserBalance
	storage.UpdateAccount(db, acc)
	if err != nil {
		return fmt.Errorf("fail to update Account with new balance by id(%s): %s",
			acc.ID.ToString(),
			err,
		)
	}

	return nil
}

// Перерасчитывает баланс аккаунта на основании всех зафиксированных транзакций
func recalculateAccountBalance(db *sql.Tx, acc *account.Account) error {
	txs, err := storage.ListTransactions(db, acc.GetID())
	if err != nil {
		return fmt.Errorf(
			"fail to list transaction by account id (%s): %s",
			acc.ID.ToString(),
			err,
		)
	}

	newUserBalance := calculate(acc, txs)
	acc.Balance = newUserBalance
	err = storage.UpdateAccount(db, acc)
	if err != nil {
		return fmt.Errorf("fail to update Account with new balance by id(%s): %s",
			acc.ID.ToString(),
			err,
		)
	}

	return nil
}

// Логика подсчета баланса на аккаунте для списка транзакций
func calculate(acc *account.Account, txs []*transaction.Transaction) *money.Money {
	res := &money.Money{}
	for i := 0; i < len(txs); i++ {
		t := txs[i]
		if t.GetType() == transaction.TransactionTypeReciept {
			if t.GetRecipientAccount().GetID() == acc.GetID() {
				res = billing_money.Summ(res, t.Amount)
			}
		}

		if t.GetType() == transaction.TransactionTypeSpend {
			if t.GetSpenderAccount().GetID() == acc.GetID() {
				res = billing_money.Diff(res, t.Amount)
			}
		}

		if t.Type == transaction.TransactionTypeTransfer {
			if t.GetRecipientAccount().GetID() == acc.GetID() {
				res = billing_money.Summ(res, t.Amount)
			}

			if t.GetSpenderAccount().GetID() == acc.GetID() {
				res = billing_money.Diff(res, t.Amount)
			}
		}

		if t.Type == transaction.TransactionTypeTransferHold {
			if t.GetSpenderAccount().GetID() == acc.GetID() {
				res = billing_money.Diff(res, t.Amount)
			}
		}
	}

	return res
}
