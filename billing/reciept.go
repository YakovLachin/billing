package billing

import (
	"database/sql"
	"fmt"

	"gitlab.com/YakovLachin/billing/billing/storage"
	"gitlab.com/YakovLachin/billing/billing/transaction"
)

func canReciept(db *sql.DB, req *transaction.Transaction) error {
	err := db.Ping()
	if err != nil {
		return fmt.Errorf("fail to ping SQL: %s", err)
	}

	recipientAcc, err := storage.GetAccount(db, req.GetRecipientAccount())
	if err != nil {
		return fmt.Errorf("fail to check recipient account with id %s : %s", req.GetRecipientAccount().GetID().ToString(), err)
	}

	if recipientAcc.IsLocked == 1 {
		return fmt.Errorf("sender account with id (%s) is locked", recipientAcc.GetID())
	}
	return nil
}

func prepareReciept(db *sql.DB, req *transaction.Transaction) (*transaction.Transaction, error) {
	req.Status = transaction.TransactionStatusProcess
	req.Type = transaction.TransactionTypeReciept
	req.SpenderAccount = nil
	res, err := storage.CreateTransaction(db, req)
	if err != nil {
		return nil, fmt.Errorf("fail to create transaction: %s", err)
	}

	return res, nil
}
