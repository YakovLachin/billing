package storage

import (
	"database/sql"
	"fmt"
	"time"

	"gitlab.com/YakovLachin/billing/billing/account"

	"gitlab.com/YakovLachin/billing/billing/transaction"

	"gitlab.com/YakovLachin/billing/billing/uuid"
	"google.golang.org/genproto/googleapis/type/money"
)

type TransactionRecord struct {
	ID        uuid.UID
	AccountID uuid.UID
	Funds     *money.Money
	Status    string
	Type      string
}

type SQLDB interface {
	Exec(query string, args ...interface{}) (sql.Result, error)
	QueryRow(query string, args ...interface{}) *sql.Row
	Query(query string, args ...interface{}) (*sql.Rows, error)
}

func CreateTransaction(db SQLDB, t *transaction.Transaction) (*transaction.Transaction, error) {
	if t == nil {
		return nil, fmt.Errorf("transaction id nil")
	}

	txID := uuid.NewUID()
	txIDString := txID.ToString()
	spenderID := t.GetSpenderAccount().GetID().ToString()
	recipientID := t.GetRecipientAccount().GetID().ToString()
	amount := t.GetAmount()
	_, err := db.Exec(
		`INSERT INTO transactions 
				(id, spender_id, recipient_id, units, nanos, currency, type_of, status, created_at) 
				VALUES (UNHEX(?), UNHEX(?), UNHEX(?), ?, ?, ?, ?, ?, ?)`,
		txIDString, spenderID, recipientID, amount.GetUnits(),
		amount.GetNanos(), amount.GetCurrencyCode(),
		t.GetType(), t.GetStatus(), time.Now(),
	)

	t.ID = txID

	if err != nil {
		return nil, fmt.Errorf("faile to exec in db: %s", err)
	}

	return t, err
}

func GetTransaction(db SQLDB, tx *transaction.Transaction) (*transaction.Transaction, error) {
	if tx.GetID().ToString() == "" {
		return nil, fmt.Errorf("faild to get trasaction uid is Empty")
	}

	row := db.QueryRow(
		"SELECT id, spender_id, recipient_id, units, nanos, currency, type_of, status FROM transactions WHERE id = UNHEX(?)",
		tx.GetID().ToString(),
	)

	t := &transaction.Transaction{}
	spenderID := uuid.UID("")
	recipientID := uuid.UID("")

	amount := money.Money{}

	err := row.Scan(&t.ID, &spenderID, &recipientID, &amount.Units, &amount.Nanos, &amount.CurrencyCode, &t.Type, &t.Status)
	if err != nil {
		return nil, fmt.Errorf("fail to scan rows: %s", err)
	}

	t.Amount = &amount
	if spenderID.ToString() != "" {
		t.SpenderAccount = &account.Account{
			ID: spenderID,
		}
	}

	if recipientID.ToString() != "" {
		t.RecipientAccount = &account.Account{
			ID: recipientID,
		}
	}

	return t, nil
}

func scanRow() {}

func ListTransactions(db SQLDB, accountID uuid.UID) ([]*transaction.Transaction, error) {
	res := []*transaction.Transaction{}
	rows, err := db.Query("SELECT units, nanos, type_of, spender_id, recipient_id FROM transactions WHERE (spender_id=UNHEX(?) OR recipient_id=UNHEX(?)) AND status=?",
		accountID.ToString(), accountID.ToString(), transaction.TransactionStatusCommit)
	if err != nil {
		return res, fmt.Errorf("fail to query from db: %s", err)
	}
	defer func() { rows.Close() }()

	for rows.Next() {
		m := &money.Money{}
		spender := &account.Account{}
		recipient := &account.Account{}
		var typeOf string
		err := rows.Scan(&m.Units, &m.Nanos, &typeOf, &spender.ID, &recipient.ID)
		if err != nil {
			return []*transaction.Transaction{}, fmt.Errorf("faile to map database data on money: %s", err)
		}
		tx := &transaction.Transaction{
			Amount:           m,
			Type:             typeOf,
			RecipientAccount: recipient,
			SpenderAccount:   spender,
		}
		res = append(res, tx)
	}

	return res, nil
}

func UpdateTransaction(db SQLDB, record *transaction.Transaction) error {
	_, err := db.Exec("UPDATE transactions SET type_of = ?, status = ? WHERE id=UNHEX(?)",
		record.GetType(), record.GetStatus(), record.GetID())
	if err != nil {
		return fmt.Errorf("faile to exec in db: %s", err)
	}

	return nil
}
