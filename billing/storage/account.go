package storage

import (
	"fmt"

	"gitlab.com/YakovLachin/billing/billing/uuid"

	"google.golang.org/genproto/googleapis/type/money"

	"gitlab.com/YakovLachin/billing/billing/account"
)

func CreateAccount(db SQLDB) (*account.Account, error) {
	newAccId := uuid.NewUID()
	_, err := db.Exec("INSERT INTO accounts (`id`) VALUES (UNHEX(?))", newAccId)
	if err != nil {
		return nil, fmt.Errorf("faild to insert new account record: %s", err)
	}

	return &account.Account{
		ID: newAccId,
	}, nil
}

func GetAccount(db SQLDB, ac *account.Account) (*account.Account, error) {
	res := &account.Account{}
	accId := ac.GetID()
	row := db.QueryRow("SELECT id, units, nanos FROM accounts WHERE id= UNHEX(?)", accId.ToString())
	m := &money.Money{}
	err := row.Scan(&res.ID, &m.Units, &m.Nanos)
	if err != nil {
		return nil, fmt.Errorf("fail to get data from db: %s", err)
	}

	res.Balance = m

	return res, nil
}

func UpdateAccount(db SQLDB, acc *account.Account) error {
	_, err := db.Exec("UPDATE accounts SET units=?, nanos=? WHERE id=UNHEX(?)",
		acc.GetBalance().GetUnits(), acc.GetBalance().Nanos, acc.GetID().ToString())
	if err != nil {
		return fmt.Errorf("fail to update account in db :%s", err)
	}

	return nil
}
