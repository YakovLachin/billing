package tests

import (
	"math/rand"
	"testing"

	"google.golang.org/genproto/googleapis/type/money"

	"gitlab.com/YakovLachin/billing/billing/account"
	"gitlab.com/YakovLachin/billing/billing/storage"
	"gitlab.com/YakovLachin/billing/billing/uuid"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/YakovLachin/billing/billing/storage/tests/helper"
)

func TestSuite(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Работа сo storage")
}

var _ = Describe("Критерии работоспособности storage", func() {
	Context("Работа с транзакциями transaction", func() {
		It("Должен получить account по uuid", func() {
			db, err := helper.GetDB()
			ExpectWithOffset(1, err).Should(BeNil())
			ExpectWithOffset(1, db).ShouldNot(BeNil())
			acc := account.Account{
				ID: uuid.UID("85731ebe56924ee592f268df12b847ca"),
			}
			res, err := storage.GetAccount(db, &acc)
			Expect(err).Should(BeNil())
			Expect(res.GetID().ToString()).Should(Equal("85731ebe56924ee592f268df12b847ca"))
		})
		It("Должен обновить account по uuid без ошибки", func() {
			db, err := helper.GetDB()
			Expect(err).Should(BeNil())
			Expect(db).ShouldNot(BeNil())
			tx, err := db.Begin()
			Expect(err).Should(BeNil())
			newBalance := &money.Money{
				Units: rand.Int63(),
				Nanos: rand.Int31(),
			}

			acc := account.Account{
				ID:      uuid.UID("85731ebe56924ee592f268df12b847ca"),
				Balance: newBalance,
			}

			err = storage.UpdateAccount(tx, &acc)
			Expect(err).Should(BeNil())
			err = tx.Commit()
			Expect(err).Should(BeNil())

			exp := &account.Account{
				ID: uuid.UID("85731ebe56924ee592f268df12b847ca"),
			}
			expected, err := storage.GetAccount(db, exp)
			Expect(expected.GetBalance()).Should(Equal(newBalance))
		})
		It("Должен обновить account по uuid без ошибки", func() {
			db, err := helper.GetDB()
			Expect(err).Should(BeNil())
			Expect(db).ShouldNot(BeNil())
			created, err := storage.CreateAccount(db)
			Expect(err).Should(BeNil())
			Expect(created.GetID()).ShouldNot(BeNil())
			Expect(created.GetID().ToString()).ShouldNot(Equal(""))

			expected, err := storage.GetAccount(db, created)
			Expect(err).Should(BeNil())
			Expect(expected.GetID()).Should(Equal(created.GetID()))
		})
	})
})
