package tests

import (
	"math/rand"

	"gitlab.com/YakovLachin/billing/billing/transaction"

	"google.golang.org/genproto/googleapis/type/money"

	"gitlab.com/YakovLachin/billing/billing/storage"
	"gitlab.com/YakovLachin/billing/billing/uuid"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/YakovLachin/billing/billing/storage/tests/helper"
)

var _ = Describe("Критерии работоспособности storage", func() {
	Context("Работа с транзакциями transaction", func() {
		var txUID uuid.UID
		var m *money.Money
		It("Должен создать транзакцию по uuid без ошибки", func() {
			db, err := helper.GetDB()
			Expect(err).Should(BeNil())
			Expect(db).ShouldNot(BeNil())

			m = &money.Money{
				Units: rand.Int63(),
				Nanos: rand.Int31(),
			}

			tx := &transaction.Transaction{
				Amount: m,
			}

			created, err := storage.CreateTransaction(db, tx)
			Expect(err).Should(BeNil())
			Expect(created.GetID()).ShouldNot(BeNil())
			Expect(created.GetID().ToString()).ShouldNot(Equal(""))
			txUID = created.GetID()
		})

		It("Должен получить созданную transaction по uuid", func() {
			db, err := helper.GetDB()
			ExpectWithOffset(1, err).Should(BeNil())
			ExpectWithOffset(1, db).ShouldNot(BeNil())
			tx := &transaction.Transaction{
				ID: txUID,
			}

			res, err := storage.GetTransaction(db, tx)
			Expect(err).Should(BeNil())
			Expect(res.GetID()).Should(Equal(txUID))
			Expect(res.GetAmount()).Should(Equal(m))
		})

		It("Должен обновлять созданную transaction по uuid", func() {
			db, err := helper.GetDB()
			ExpectWithOffset(1, err).Should(BeNil())
			ExpectWithOffset(1, db).ShouldNot(BeNil())
			tx := &transaction.Transaction{
				ID: txUID,
			}

			res, err := storage.GetTransaction(db, tx)
			Expect(err).Should(BeNil())
			Expect(res.GetID()).Should(Equal(txUID))
			Expect(res.GetAmount()).Should(Equal(m))
		})

	})
})
