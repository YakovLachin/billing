package billing

import (
	"database/sql"
	"fmt"

	"gitlab.com/YakovLachin/billing/billing/storage"
	"gitlab.com/YakovLachin/billing/billing/transaction"
)

// Проводит проверку на соединение с б.д.
// существование аккаунтов
// и возможность взаимодействия с ними
func canTransfer(db *sql.DB, req *transaction.Transaction) error {
	err := db.Ping()
	if err != nil {
		return fmt.Errorf("fail to ping SQL: %s", err)
	}

	spenderAcc, err := storage.GetAccount(db, req.GetSpenderAccount())
	if err != nil {
		return fmt.Errorf("fail to check spender account with id %s : %s", req.GetSpenderAccount().GetID().ToString(), err)
	}

	if spenderAcc.IsLocked == 1 {
		return fmt.Errorf("spender account with id (%s) is locked", spenderAcc.GetID())
	}

	recipientAcc, err := storage.GetAccount(db, req.GetRecipientAccount())
	if err != nil {
		return fmt.Errorf("fail to check recipient account with id %s : %s", req.GetRecipientAccount().GetID().ToString(), err)
	}

	if recipientAcc.IsLocked == 1 {
		return fmt.Errorf("recipient account with id (%s) is locked", recipientAcc.GetID())
	}

	return nil
}

// подготавливается транзакцию для перевода
func prepareTransfer(db *sql.DB, req *transaction.Transaction) (*transaction.Transaction, error) {
	req.Status = transaction.TransactionStatusProcess
	req.Type = transaction.TransactionTypeTransfer
	res, err := storage.CreateTransaction(db, req)
	if err != nil {
		return nil, fmt.Errorf("fail to create transaction: %s", err)
	}

	return res, nil
}
