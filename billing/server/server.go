package server

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"

	"gitlab.com/YakovLachin/billing/billing/account"

	"gitlab.com/YakovLachin/billing/amqp"
	"gitlab.com/YakovLachin/billing/billing/transaction"
)

// AMQP Запрос на транзакцию
type AMQPTransactionRequest struct {
	RPCName string                   `json:"rpc_name"`
	Body    *transaction.Transaction `json:"body"`
}

// AMQP Ответ amqp транзакцию
type AMQPTransactionResponse struct {
	RPCName string                   `json:"rpc_name"`
	Body    *transaction.Transaction `json:"body"`
}

// структура AMQP ошибки по одному из запросов
type AMQPRequestError struct {
	// Код ошибки.
	ErrorCode string `json:"error_code"`
	// Сообщение из ошибки
	ErrorMessage string `json:"error_message"`
	// Запрос на который была выброшена ошибка
	Request interface{} `json:"request"`
}

// тип функции обработчик для биллинга
// все обработчики вызовов должны соответсвовать данному типу
type billingHandleFunc func(*sql.DB, *transaction.Transaction) (*transaction.Transaction, error)
type accountHandleFunc func(*sql.DB, *account.Account) (*account.Account, error)

// AMQPHandleTxFunc конфигурирует адаптер для функции mux amqp обработчика
func AMQPHandleTxFunc(db *sql.DB, handleFunc billingHandleFunc) amqp.HandleFunc {
	return func(payload []byte) ([]byte, error) {
		resp := AMQPTransactionResponse{}
		t := &AMQPTransactionRequest{}
		err := json.Unmarshal(payload, t)
		if err != nil {
			return []byte{}, AMQPRequestERR(t, fmt.Errorf("fail to unmarshal request: %s", err))
		}

		tx, err := handleFunc(db, t.Body)
		if err != nil {
			return []byte{}, AMQPRequestERR(t, err)
		}

		resp.Body = tx
		msg, err := json.Marshal(resp)
		if err != nil {
			return []byte{}, AMQPRequestERR(t, fmt.Errorf("fail to marshal response: %s", err))
		}

		return msg, err
	}
}

type AMQPAccountRequest struct {
	RPCName string           `json:"rpc_name"`
	Body    *account.Account `json:"body"`
}

type AMQPAccountResponse struct {
	RPCName string           `json:"rpc_name"`
	Body    *account.Account `json:"body"`
}

func AMQPHandleAccountFunc(db *sql.DB, handleFunc accountHandleFunc) amqp.HandleFunc {
	return func(payload []byte) ([]byte, error) {
		resp := AMQPAccountResponse{}
		t := &AMQPAccountRequest{}
		err := json.Unmarshal(payload, t)
		if err != nil {
			return []byte{}, AMQPRequestERR(t, fmt.Errorf("fail to unmarshal request: %s", err))
		}

		tx, err := handleFunc(db, t.Body)
		if err != nil {
			return []byte{}, AMQPRequestERR(t, err)
		}

		resp.Body = tx
		msg, err := json.Marshal(resp)
		if err != nil {
			return []byte{}, AMQPRequestERR(t, fmt.Errorf("fail to marshal response: %s", err))
		}

		return msg, err
	}
}

func AMQPRequestERR(req interface{}, err error) error {
	amqpError := &AMQPRequestError{
		ErrorMessage: err.Error(),
		Request:      req,
	}

	errJSON, _ := json.Marshal(amqpError)
	return errors.New(string(errJSON))
}
