package billing

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/YakovLachin/billing/billing/account"
	"gitlab.com/YakovLachin/billing/billing/transaction"
	"gitlab.com/YakovLachin/billing/billing/uuid"
	"google.golang.org/genproto/googleapis/type/money"
)

func TestCalculate_TransactionsIsSpendsOnly_ReturnNegative(t *testing.T) {
	accUID := uuid.NewUID()
	txs := []*transaction.Transaction{
		{
			SpenderAccount: &account.Account{
				ID: accUID,
			},
			Type: transaction.TransactionTypeSpend,
			Amount: &money.Money{
				Units: 100,
				Nanos: 500000000,
			},
		},
		{
			SpenderAccount: &account.Account{
				ID: accUID,
			},
			Type: transaction.TransactionTypeSpend,
			Amount: &money.Money{
				Units: 100,
				Nanos: 500000000,
			},
		},
		{
			SpenderAccount: &account.Account{
				ID: accUID,
			},
			Type: transaction.TransactionTypeSpend,
			Amount: &money.Money{
				Units: 100,
				Nanos: 500000000,
			},
		},
		{
			SpenderAccount: &account.Account{
				ID: accUID,
			},
			Type: transaction.TransactionTypeSpend,
			Amount: &money.Money{
				Units: 100,
				Nanos: 500000000,
			},
		},
	}

	acc := &account.Account{
		ID: accUID,
	}
	b := calculate(acc, txs)
	assert.Equal(t, b, &money.Money{Units: -402, Nanos: 0})
}

func TestCalculate_TransactionsIsRecieptOnly_ReturnSumm(t *testing.T) {
	accUID := uuid.NewUID()
	txs := []*transaction.Transaction{
		{
			RecipientAccount: &account.Account{
				ID: accUID,
			},
			Type: transaction.TransactionTypeReciept,
			Amount: &money.Money{
				Units: 100,
				Nanos: 500000000,
			},
		},
		{
			RecipientAccount: &account.Account{
				ID: accUID,
			},
			Type: transaction.TransactionTypeReciept,
			Amount: &money.Money{
				Units: 100,
				Nanos: 500000000,
			},
		},
		{
			RecipientAccount: &account.Account{
				ID: accUID,
			},
			Type: transaction.TransactionTypeReciept,
			Amount: &money.Money{
				Units: 100,
				Nanos: 500000000,
			},
		},
		{
			RecipientAccount: &account.Account{
				ID: accUID,
			},
			Type: transaction.TransactionTypeReciept,
			Amount: &money.Money{
				Units: 100,
				Nanos: 500000000,
			},
		},
	}

	acc := &account.Account{
		ID: accUID,
	}
	b := calculate(acc, txs)
	assert.Equal(t, b, &money.Money{Units: 402, Nanos: 0})
}

func TestCalculate_TransactionsIsRecieptAndSpend_ReturnZero(t *testing.T) {
	accUID := uuid.NewUID()
	txs := []*transaction.Transaction{
		{
			RecipientAccount: &account.Account{
				ID: accUID,
			},
			Type: transaction.TransactionTypeReciept,
			Amount: &money.Money{
				Units: 100,
				Nanos: 500000000,
			},
		},
		{
			SpenderAccount: &account.Account{
				ID: accUID,
			},
			Type: transaction.TransactionTypeSpend,
			Amount: &money.Money{
				Units: 100,
				Nanos: 500000000,
			},
		},
		{
			RecipientAccount: &account.Account{
				ID: accUID,
			},
			Type: transaction.TransactionTypeReciept,
			Amount: &money.Money{
				Units: 100,
				Nanos: 500000000,
			},
		},
		{
			SpenderAccount: &account.Account{
				ID: accUID,
			},
			Type: transaction.TransactionTypeSpend,
			Amount: &money.Money{
				Units: 100,
				Nanos: 500000000,
			},
		},
	}

	acc := &account.Account{
		ID: accUID,
	}
	b := calculate(acc, txs)
	assert.Equal(t, b, &money.Money{Units: 0, Nanos: 0})
}

func TestCalculate_RecieptAndTransfer(t *testing.T) {
	accUID := uuid.NewUID()
	accUID2 := uuid.NewUID()
	txs := []*transaction.Transaction{
		{
			RecipientAccount: &account.Account{
				ID: accUID,
			},
			Type: transaction.TransactionTypeReciept,
			Amount: &money.Money{
				Units: 100,
				Nanos: 200000000,
			},
		},
		{
			RecipientAccount: &account.Account{
				ID: accUID2,
			},
			Type: transaction.TransactionTypeReciept,
			Amount: &money.Money{
				Units: 100,
				Nanos: 200000000,
			},
		},
		{
			SpenderAccount: &account.Account{
				ID: accUID,
			},
			RecipientAccount: &account.Account{
				ID: accUID2,
			},
			Type: transaction.TransactionTypeTransfer,
			Amount: &money.Money{
				Units: 100,
				Nanos: 200000000,
			},
		},
	}

	acc := &account.Account{
		ID: accUID,
	}
	b := calculate(acc, txs)
	assert.Equal(t, b, &money.Money{Units: 0, Nanos: 0})
}
