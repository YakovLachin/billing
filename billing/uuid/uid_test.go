package uuid

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUID_Value_UIDIsFull_ReturnsByteArray(t *testing.T) {
	uid := UID("85731ebe56924ee592f268df12b847ca")
	val, err := uid.Value()
	assert.Nil(t, err)
	assert.Equal(t, len(val.([]byte)), 16)
}
