package uuid

import (
	"database/sql/driver"
	"encoding/hex"
	"errors"
	"fmt"

	"github.com/google/uuid"
)

type UID string

func NewUID() UID {
	uid := uuid.New()
	s := fmt.Sprintf("%x%x%x%x%x", uid[0:4], uid[4:6], uid[6:8], uid[8:10], uid[10:])

	return UID(s)
}

func (uid *UID) Scan(value interface{}) error {
	bytes, ok := value.([]byte)
	if !ok {
		return fmt.Errorf("Ошибка получение uid из Б.Д. %v", value)
	}

	*uid = UID(hex.EncodeToString(bytes))

	return nil
}

// Value implements the driver Valuer interface.
func (uid *UID) Value() (driver.Value, error) {
	if len(*uid) == 0 {
		return nil, errors.New("UID is Empty")
	}

	return hex.DecodeString(string(*uid))
}

func (uid UID) ToString() string {
	return string(uid)
}
