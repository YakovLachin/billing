package account

import (
	"gitlab.com/YakovLachin/billing/billing/uuid"
	"google.golang.org/genproto/googleapis/type/money"
)

type Account struct {
	ID       uuid.UID     `json:"id"`
	Balance  *money.Money `json:"balance"`
	IsLocked int          `json:"-"`
}

func (a *Account) GetID() uuid.UID {
	if a == nil {
		return uuid.UID("")
	}

	return a.ID
}

func (a *Account) GetBalance() *money.Money {
	if a == nil {
		return nil
	}

	return a.Balance
}

func (a *Account) GetIsLocked() int {
	if a == nil {
		return 0
	}

	return a.IsLocked
}
