package billing

import (
	"database/sql"
	"fmt"

	"gitlab.com/YakovLachin/billing/billing/storage"
	"gitlab.com/YakovLachin/billing/billing/transaction"
)

func prepareTransferHold(db *sql.DB, req *transaction.Transaction) (*transaction.Transaction, error) {
	req.Status = transaction.TransactionStatusProcess
	req.Type = transaction.TransactionTypeTransferHold
	res, err := storage.CreateTransaction(db, req)
	if err != nil {
		return nil, fmt.Errorf("fail to create transaction: %s", err)
	}

	return res, nil
}

// Перевеодит холдированный платеж
func confirmTransferHold(db *sql.DB, prepared *transaction.Transaction) (err error) {
	if prepared.GetSpenderAccount() == nil && prepared.GetRecipientAccount() == nil {
		return fmt.Errorf(
			"transaction with id(%s): sender and recipient is empty",
			prepared.ID.ToString(),
		)
	}

	prepared.Type = transaction.TransactionTypeTransfer
	err = storage.UpdateTransaction(db, prepared)
	if err != nil {
		return fmt.Errorf("fail to update transaction with id(%s) to status commit: %s", prepared.ID.ToString(), err)
	}

	tx, txErr := db.Begin()
	if txErr != nil {
		err = fmt.Errorf(
			"transaction with id(%s): fail to begin transaction: %s",
			prepared.ID.ToString(),
			txErr,
		)
	}

	defer func() {
		if err != nil {
			rollBackErr := tx.Rollback()
			if rollBackErr != nil {
				err = fmt.Errorf("fail rollback transaction %s, after error (%s)", rollBackErr, err)
			}
		}
	}()

	if prepared.GetSpenderAccount() != nil {
		err := recalculateAccountBalance(tx, prepared.GetSpenderAccount())
		if err != nil {
			return fmt.Errorf(
				"fail to commit transaction by id(%s): fail to recalculate spender balanse: %s",
				prepared.ID.ToString(),
				err,
			)
		}
	}

	if prepared.GetRecipientAccount() != nil {
		err := recalculateAccountBalance(tx, prepared.GetRecipientAccount())
		if err != nil {
			return fmt.Errorf(
				"fail to commit transaction by id(%s): fail to recalculate recipient balanse: %s",
				prepared.ID.ToString(),
				err,
			)
		}
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("fial to commit transaction with id(%s): %s", prepared.ID.ToString(), err)
	}

	return nil
}

func rollbackTransferHold(db *sql.DB, prepared *transaction.Transaction) (err error) {
	if prepared.GetSpenderAccount() == nil && prepared.GetRecipientAccount() == nil {
		return fmt.Errorf(
			"transaction with id(%s): sender and recipient is empty",
			prepared.ID.ToString(),
		)
	}

	prepared.Status = transaction.TransactionStatusRollback

	fmt.Printf("%v", *prepared)
	err = storage.UpdateTransaction(db, prepared)
	if err != nil {
		return fmt.Errorf("fail to update transaction with id(%s) to status commit: %s", prepared.ID.ToString(), err)
	}

	tx, txErr := db.Begin()
	if txErr != nil {
		err = fmt.Errorf(
			"transaction with id(%s): fail to begin transaction: %s",
			prepared.ID.ToString(),
			txErr,
		)
	}

	defer func() {
		if err != nil {
			rollBackErr := tx.Rollback()
			if rollBackErr != nil {
				err = fmt.Errorf("fail rollback transaction %s, after error (%s)", rollBackErr, err)
			}
		}
	}()

	if prepared.GetSpenderAccount() != nil {
		err := recalculateAccountBalance(tx, prepared.GetSpenderAccount())
		if err != nil {
			return fmt.Errorf(
				"fail to commit transaction by id(%s): fail to recalculate spender balanse: %s",
				prepared.ID.ToString(),
				err,
			)
		}
	}

	if prepared.GetRecipientAccount() != nil {
		err := recalculateAccountBalance(tx, prepared.GetRecipientAccount())
		if err != nil {
			return fmt.Errorf(
				"fail to commit transaction by id(%s): fail to recalculate recipient balanse: %s",
				prepared.ID.ToString(),
				err,
			)
		}
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("fial to commit transaction with id(%s): %s", prepared.ID.ToString(), err)
	}

	return nil
}

func getHold(db *sql.DB, req *transaction.Transaction) (*transaction.Transaction, error) {
	res, err := storage.GetTransaction(db, req)
	if err != nil {
		return nil, fmt.Errorf("fail to get transaction: %s", err)
	}

	if res.Type != transaction.TransactionTypeTransferHold {
		return nil, fmt.Errorf("fail to get transaction by id %s: transaction is not hold", req.GetID().ToString())
	}

	if res.GetRecipientAccount().GetID() != req.GetRecipientAccount().GetID() {
		return nil, fmt.Errorf("fail to get transaction by id %s: access denied", req.GetID().ToString())
	}

	return res, nil
}
