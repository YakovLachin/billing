package billing

import (
	"database/sql"
	"fmt"

	"gitlab.com/YakovLachin/billing/billing/transaction"

	"gitlab.com/YakovLachin/billing/billing/storage"
)

func canSpend(db *sql.DB, req *transaction.Transaction) error {
	err := db.Ping()
	if err != nil {
		return fmt.Errorf("fail to ping SQL: %s", err)
	}

	senderAcc, err := storage.GetAccount(db, req.SpenderAccount)
	if err != nil {
		return fmt.Errorf("fail to check spender account with id %s : %s", req.SpenderAccount.GetID().ToString(), err)
	}

	if senderAcc.IsLocked == 1 {
		return fmt.Errorf("sender account with id (%s) is locked", senderAcc.GetID())
	}

	return nil
}

// Подготавливает транзакцию на списание денежных средств.
func prepareSpend(db *sql.DB, req *transaction.Transaction) (*transaction.Transaction, error) {
	req.Status = transaction.TransactionStatusProcess
	req.Type = transaction.TransactionTypeSpend
	req.RecipientAccount = nil
	res, err := storage.CreateTransaction(db, req)
	if err != nil {
		return nil, fmt.Errorf("fail to create transaction: %s", err)
	}

	return res, nil
}
