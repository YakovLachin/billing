package billing

import (
	"database/sql"
	"fmt"
)

func canSignup(db *sql.DB) error {
	err := db.Ping()
	if err != nil {
		return fmt.Errorf("fail to ping SQL: %s", err)
	}
	return nil
}
