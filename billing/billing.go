package billing

import (
	"database/sql"

	"gitlab.com/YakovLachin/billing/billing/storage"

	"gitlab.com/YakovLachin/billing/billing/account"

	"gitlab.com/YakovLachin/billing/billing/transaction"
)

func Reciept(db *sql.DB, request *transaction.Transaction) (*transaction.Transaction, error) {
	err := canReciept(db, request)
	if err != nil {

		return nil, err
	}

	reciept, err := prepareReciept(db, request)
	if err != nil {

		return nil, err
	}

	err = commitTransaction(db, reciept)
	if err != nil {

		return nil, err
	}

	return reciept, nil
}

func Spend(db *sql.DB, request *transaction.Transaction) (*transaction.Transaction, error) {
	err := canSpend(db, request)
	if err != nil {
		return nil, err

	}

	spend, err := prepareSpend(db, request)
	if err != nil {
		return nil, err
	}

	err = commitTransaction(db, spend)
	if err != nil {
		return nil, err
	}

	return spend, nil
}

func Transfer(db *sql.DB, request *transaction.Transaction) (*transaction.Transaction, error) {
	err := canTransfer(db, request)
	if err != nil {
		return nil, err
	}

	transfer, err := prepareTransfer(db, request)
	if err != nil {
		return nil, err
	}

	err = commitTransaction(db, transfer)
	if err != nil {
		return nil, err
	}

	return transfer, nil
}

func TransferHold(db *sql.DB, request *transaction.Transaction) (*transaction.Transaction, error) {
	err := canTransfer(db, request)
	if err != nil {
		return nil, err
	}

	transfer, err := prepareTransferHold(db, request)
	if err != nil {
		return nil, err
	}

	err = commitTransaction(db, transfer)
	if err != nil {
		return nil, err
	}

	return transfer, nil
}

func TransferConfirm(db *sql.DB, request *transaction.Transaction) (*transaction.Transaction, error) {
	err := canTransfer(db, request)
	if err != nil {
		return nil, err
	}

	hold, err := getHold(db, request)
	if err != nil {
		return nil, err
	}

	err = confirmTransferHold(db, hold)
	if err != nil {
		return nil, err
	}

	return hold, nil
}

func TransferRollback(db *sql.DB, request *transaction.Transaction) (*transaction.Transaction, error) {
	err := canTransfer(db, request)
	if err != nil {
		return nil, err
	}

	hold, err := getHold(db, request)
	if err != nil {
		return nil, err
	}

	err = rollbackTransferHold(db, hold)
	if err != nil {
		return nil, err
	}

	return hold, nil
}

func Signup(db *sql.DB, _ *account.Account) (*account.Account, error) {
	err := canSignup(db)
	if err != nil {
		return nil, err
	}

	acc, err := storage.CreateAccount(db)
	if err != nil {
		return nil, err
	}

	return acc, nil
}
