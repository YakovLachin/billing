FROM golang:1.10-alpine as builder
ARG WORKDIR
WORKDIR /go/src/$WORKDIR
COPY . .
RUN GOOS=linux GOARCH=amd64 go build -v -o bin/service

FROM golang:1.10-alpine
ARG IMAGE
ARG WORKDIR
WORKDIR /service
COPY --from=builder /go/src/$WORKDIR/bin/service .
CMD ./service
