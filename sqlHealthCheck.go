package main

import (
	"database/sql"
	"fmt"
	"log"
	"time"
)

func SqlHealthCheck(sql *sql.DB, mysqlDialURL string, done <-chan struct{}) {
	for {
		select {
		case <-done:
			return
		default:
			time.Sleep(5 * time.Second)
			err := sql.Ping()
			if err == nil {
				log.Printf("Mysql Health Check: success")
				continue
			}
			sql.Close()
			log.Printf("Mysql Health Check: failed")
			db, tryErr := TryOpenSQL(mysqlDialURL)
			if tryErr != nil {
				log.Printf("Reconnect to mysql: failed")
				continue
			}
			log.Printf("Reconnect to mysql: success")
			*sql = *db
		}
	}
}

func TryOpenSQL(mysqlDialURL string) (*sql.DB, error) {
	for i := 10; i > 0; i-- {
		db, err := sql.Open("mysql", mysqlDialURL)
		if err == nil {
			return db, nil
		}
		time.Sleep(time.Second)
	}
	return nil, fmt.Errorf("fail to open mysql connection by dial:%s", mysqlDialURL)
}
