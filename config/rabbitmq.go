package config

import (
	"errors"
	"fmt"
	"os"
)

type rabbitMQDialConfig struct {
	scheme string
	host   string
	port   string
	login  string
	pass   string
	vhost  string
}

func (c rabbitMQDialConfig) ToStringURL() string {
	return fmt.Sprintf("%s://%s:%s@%s:%s", c.scheme, c.login, c.pass, c.host, c.port)
}

func getConfigFromEnv() (*rabbitMQDialConfig, error) {
	scheme := os.Getenv("RABBITMQ_SCHEME")
	if scheme == "" {
		scheme = "amqp"
	}

	host := os.Getenv("RABBITMQ_HOST")
	if host == "" {
		return nil, errors.New("fail to get amqp from RABBITMQ_HOST env")
	}
	port := os.Getenv("RABBITMQ_PORT")
	if port == "" {
		return nil, errors.New("fail to get amqp from RABBITMQ_PORT env")
	}

	user := os.Getenv("RABBITMQ_USER")
	if user == "" {
		return nil, errors.New("fail to get amqp from RABBITMQ_USER env")
	}

	pass := os.Getenv("RABBITMQ_PASS")
	if pass == "" {
		return nil, errors.New("fail to get amqp from RABBITMQ_PASS env")
	}
	vhost := os.Getenv("RABBITMQ_VHOST")
	if vhost == "" {
		vhost = "/"
	}

	return &rabbitMQDialConfig{
		scheme: scheme,
		host:   host,
		port:   port,
		login:  user,
		pass:   pass,
		vhost:  vhost,
	}, nil
}

// Возвращает DialUrl для создания соединения
// c RabbitMQ
func GetRabbitMQDialURL() (string, error) {
	dialConfig, err := getConfigFromEnv()
	if err != nil {
		return "", err
	}

	return dialConfig.ToStringURL(), nil
}

// GetInputQueueName Возвращает имя очереди для заданий
func GetInputQueueName() string {
	queue := os.Getenv("BILLING_INPUT_QUEUE")
	if queue == "" {
		queue = "default_input"
	}

	return queue
}

// GetOutputQueueName Возвращает имя очереди для возвращения реузлтата заданий
func GetOutputQueueName() string {
	queue := os.Getenv("BILLING_OUTPUT_QUEUE")
	if queue == "" {
		queue = "default_output"
	}

	return queue
}

// GetOutputQueueName Возвращает имя очереди для возвращения реузлтата заданий
func GetErrorOutputQueueName() string {
	queue := os.Getenv("BILLING_ERROR_OUTPUT_QUEUE")
	if queue == "" {
		queue = "default_error_output"
	}

	return queue
}
