package config

import "fmt"

type MySQLConfig struct {
	Host   string
	Port   string
	User   string
	DBName string
	Pass   string
}

func NewMysqlConfigFromEnv(prefix string) (*MySQLConfig, error) {
	host, err := GetConfigValue(prefix + "_MYSQL_HOST")
	if err != nil {
		return nil, err
	}
	port, err := GetConfigValue(prefix + "_MYSQL_PORT")
	if err != nil {
		return nil, err
	}
	user, err := GetConfigValue(prefix + "_MYSQL_USER")
	if err != nil {
		return nil, err
	}
	dbname, err := GetConfigValue(prefix + "_MYSQL_DBNAME")
	if err != nil {
		return nil, err
	}
	pass, err := GetConfigValue(prefix + "_MYSQL_PASS")
	if err != nil {
		return nil, err
	}

	return &MySQLConfig{
		Host:   host,
		DBName: dbname,
		Pass:   pass,
		User:   user,
		Port:   port,
	}, nil
}

func (c *MySQLConfig) String() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=true&loc=Local", c.User, c.Pass, c.Host, c.Port, c.DBName)
}

// Возвращает DialUrl для создания соединения
// c RabbitMQ
func GetMYSQLDialURL() (string, error) {
	c, err := NewMysqlConfigFromEnv("BILLING")
	if err != nil {
		return "", err
	}

	return c.String(), nil
}
