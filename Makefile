IMAGE=gitlab.com/yakovlachin/billing
WORKDIR=gitlab.com/YakovLachin/billing
CWD=/go/src/gitlab.com/YakovLachin/billing
NETWORK=billing-network

# Собирает docker ящик с сервисом.
image:
	@docker build --build-arg IMAGE=$(IMAGE) --build-arg WORKDIR=$(WORKDIR) -t $(IMAGE) .

# Поднять собранный сервис
up: dependence
	@docker-compose up -d

# Поднять зависимые сервисы для работы сервиса
dependence: network
	@docker-compose -f docker-compose.dependence.yml up -d

# Поднять сеть для работы всех сервисов в одной сети
network:
	@-docker network create $(NETWORK)

# Запустить приёмочное тестирование
acceptance:
	@docker run --rm \
	--network billing-network \
	-v $(CURDIR):$(CWD) -w $(CWD) \
    -e RABBITMQ_HOST=amqp \
    -e RABBITMQ_PORT=5672 \
    -e RABBITMQ_USER=guest \
    -e RABBITMQ_PASS=guest \
    -e BILLING_INPUT_QUEUE=billing_input \
    -e BILLING_OUTPUT_QUEUE=billing_output \
    -e BILLING_OUTPUT_FAILED_QUEUE=billing_failed_queue \
    -e BILLING_MYSQL_HOST=mysql \
    -e BILLING_MYSQL_PORT=3306 \
    -e BILLING_MYSQL_USER=root \
    -e BILLING_MYSQL_DBNAME=billing \
    -e BILLING_MYSQL_PASS=qwerty \
	golang:1.10-alpine \
	sh -c "go test -v ./acceptance ./billing/storage/tests"

# Запустить Unit тесты
unit:
	@docker run --rm \
	--network billing-network \
	-v $(CURDIR):$(CWD) -w $(CWD) \
	golang:1.10-alpine \
	sh -c "go test -v ./billing ./billing/uuid ./billing/money ./amqp"

test: unit acceptance

down:
	@docker-compose down -v --rmi all
	@docker-compose -f docker-compose.dependence.yml down -v --rmi all

.PHONY: acceptance