CREATE DATABASE IF NOT EXISTS `billing`
  COLLATE 'utf8_general_ci'
  DEFAULT CHARSET 'utf8';

CREATE TABLE `accounts` (
    `id`   BINARY(16) NOT NULL COMMENT 'Идентификатор аккаунта.',
    `units` BIGINT DEFAULT 0 COMMENT 'целая часть баланса',
    `nanos` INT    DEFAULT 0 COMMENT 'остатки 750 000 000 есть .75 от суммы',
    PRIMARY KEY `balance_uid` (`id`)
);

CREATE TABLE `transactions` (
    `id`         BINARY(16) NOT NULL COMMENT 'Идентификатор транзакции.',
    `units`       BIGINT COMMENT 'целая часть баланса',
    `nanos`       INT COMMENT 'остатки 750 000 000 есть .75 от суммы',
    `created_at`  DATETIME COMMENT 'время создания транзакции',
    `spender_id`  BINARY(16) COMMENT 'Идентификатор отправляющего.',
    `recipient_id`BINARY(16) COMMENT 'Идентификатор принимающего.',
    `currency`    VARCHAR (255) COMMENT 'код валюты',
    `type_of`     VARCHAR (255) COMMENT 'тип транзакции например: spend - списание средств',
    `status`      VARCHAR (255) COMMENT 'статус тразакции например: proccess -  в процессе работы',
    PRIMARY KEY `balance_uid` (`id`),
    INDEX (`spender_id`),
    INDEX (`recipient_id`)
);

INSERT INTO `accounts` (`id`, `units`, `nanos`)
    VALUES (UNHEX("85731ebe56924ee592f268df12b847ca"), 0, 0);
